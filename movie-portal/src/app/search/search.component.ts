import { Component } from '@angular/core';
import {MovieService} from '../service/movieservice.service'
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  constructor(private movieservice : MovieService) { }
  
//This function is triggered on keyup
  search=(searchword)=>{
      this.movieservice.getsearchkey(searchword);
  }


}
