import { Component, OnInit,OnDestroy,Output,EventEmitter } from '@angular/core';
import { MovieService } from '../service/movieservice.service';


import { Subscription } from 'rxjs/Subscription';
@Component({
    selector : 'app-list',
    templateUrl : './list.component.html',
    styleUrls  : ['./list.component.css']
})

export class ListComponent implements OnInit,OnDestroy{
    @Output() gettingId = new EventEmitter();
    movies :any;
    movieDetailObject :any='';
    term :string='';
    subscriber : Subscription;
    imgURL : string='http://image.tmdb.org/t/p/original/';
    constructor(private _movieservice : MovieService){}

    ngOnInit() {
        //Getting all the movies from service
        this._movieservice.getMovies()
        .subscribe((movies)=>{
            this.movies = movies.results
            console.log(this.movies)
        },
    (error)=>{
        debugger
        console.error(error)
    })
        //Getting serach key word from search component through subject and passing it to the listfilter pipe args
        this.subscriber = this._movieservice.searchkey.subscribe(msg=>{
            console.log( msg )
            this.term = msg
        })
    }

    //getting the movie based ion id from service and triggering a event to pass it to app Component
    getId=(id)=>{
        this._movieservice.getMovieById(id)
        .subscribe((movie)=>{
            this.movieDetailObject = movie;
            if(this.movieDetailObject){
                this.gettingId.emit(this.movieDetailObject)
            }
        },
    error=>{
        console.error(error);
    });

    }
    //Unsubscribing to prevent memory leakage
    ngOnDestroy(){
        this.subscriber.unsubscribe();
    }
}
