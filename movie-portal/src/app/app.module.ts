import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http'
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import {MovieService} from './service/movieservice.service';
import {ListFilterPipe} from './custom-pipe/listfilter.pipe';
import { WrapTextPipe } from './custom-pipe/wraptext.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ListComponent,
    DetailComponent,
    ListFilterPipe,
    WrapTextPipe
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
