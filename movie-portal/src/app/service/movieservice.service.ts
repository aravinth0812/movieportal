import { Http } from '@angular/http';
import {Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from "rxjs/BehaviorSubject";
@Injectable()

export class MovieService{
    baseUrl:string = 'https://api.themoviedb.org/3/movie/';
    key:string ='5bc876bdf6b575b0a4bafa67453b0b2e';
constructor(private http : Http){}

     searchkey = new BehaviorSubject<string>('');

//This will update the search keyword in search component and passes this to list component using subject
    getsearchkey= (value : string)=>{
        this.searchkey.next(value);
    }

    //Get all movies from the server
    getMovies= () =>{
        return this.http.get(`${this.baseUrl}popular?api_key=${this.key}`)
         .map(response => response.json()) 
    }

    //Get  movie from the server based on ID
    getMovieById= (id) =>{
        return this.http.get(`${this.baseUrl}${id}?api_key=${this.key}`)
         .map(response => response.json());
    }
}
