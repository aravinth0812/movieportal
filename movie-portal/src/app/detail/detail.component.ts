import { Component,Input,OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector : 'app-detail',
    templateUrl : './detail.component.html',
    styleUrls : ['./detail.component.css']
})

export class DetailComponent implements OnChanges{
@Input() MovieDetailObject : any;

image : string;
constructor(){
    console.log(this.MovieDetailObject);
 }
 
 ngOnChanges(changes: SimpleChanges) {
    if(changes.MovieDetailObject) {
     window.scrollTo(0, 0)
     this.image ='http://image.tmdb.org/t/p/original' + changes.MovieDetailObject.currentValue.backdrop_path
     console.log(this.image)
    }
  }
}