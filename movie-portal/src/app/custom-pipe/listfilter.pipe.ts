import {Pipe,PipeTransform} from '@angular/core'
@Pipe({
    name : 'listFilter'
})

export class ListFilterPipe implements PipeTransform {
    transform(moviesList: any[], term :string) {
        return moviesList
            ? moviesList.filter(movie => movie.title.toLowerCase().indexOf(term.toLowerCase()) !== -1)
            : moviesList;
        
    }
}

