import {Pipe,PipeTransform} from '@angular/core'
@Pipe({
    name : 'wrapText'
})

export class WrapTextPipe implements PipeTransform {
    transform(movieDesc: any[]) {
        return movieDesc.slice(0, 100) + '...';
        
    }
}

